'use strict';

const Printers = require('../Printers')

Printers(test(),{type:'usb',VID:0x04B8,PID:0x0E0F},function (res) {
    if(res.success){
        console.log(res);
        console.log('打印成功!')
    }else{
        console.log(res);
        console.log('打印失败')
    }
})
// Printers(test(),{type:'net',ip:'172.28.13.111',port:9100},function (res) {
//     if(res.success){
//         console.log(res);
//         console.log('打印成功!')
//     }else{
//         console.log(res);
//         console.log('打印失败')
//     }
// })

function test() {
    return {
        title: '上南路测试店',
        num: '饿了吗' + '10',
        storename: '测试店',
        orderid: '1321321354613134646',
        thr_orderid: 'SDFGDFG4545456D4F513135',
        ordertime: '2018-01-04 09:23:12',
        appointment_time: '2018-01-04 09:23:12',
        conf_time: new Date().toLocaleString(), // 打印时间
        username: '测试', // 顾客姓名
        phone: '17612187024', // 顾客电话
        address: '测试测试测试', // 送货地址
        receivable_amount: 0, // 应收现金
        invoice: '不需要发票', // 发票抬头
        remarks: '', // 备注
        waiter: '某某某', // 骑手
        productList: [
            {
                'title': '测试菜试菜品试菜品试菜品试菜品试菜品品踩踩踩',
                'num': 1,
                'total_price': 20.00,
                'description': ''
            },
            {
                'title': '名字长一点1',
                'num': 20,
                'total_price': 515.00,
                'description': ''
            },
            {
                'title': '测试测试',
                'num': 10,
                'total_price': 40.00,
                'description': ''
            }
        ],
        order_amout: 100.00,
        order_discount: 2.00,
        discount_detail: [
            '美团优惠5元',
            '红包优惠2元'
        ],
        thr_amout: 70.00,
        pay_status: '未支付',
        description: [
            '欢迎到' + '上南路测试店' + '用餐',
            '电话 (' + '17612187024' + ')',
            '如您的付款与小票不一致',
            '请拨打财务监督电话 (157106529856)'
        ]
    };
}
