'use strict';
const escpos = require('../');

const device  = new escpos.USB(0x04B8, 0x0E0F);

const printer = new escpos.Printer(device);


// const device  = new escpos.Network('172.28.13.111');
// const printer = new escpos.Printer(device);


device.open(function() {
  printer
  .model('qsprinter')
  .font('a')
  .align('ct')
  .size(1, 1)
  .encode('utf8')
  .text('QR code example')
  // .qrcodeqs('http://agriex.market')
  .qrcode('ทดสอบhttp://agriex.market')
  // .barcode('123456789012', 'EAN13') // code length 12
  // .barcode('109876543210') // default type 'EAN13'
  // .barcode('7654321', 'EAN8') // The EAN parity bit is automatically added.
  .close();
});
