/**
 * Created by 27260 on 2018/1/8.
 */
'use strict';
const escpos = require('./');
const ASB = require('./adapter/ASB');
const Active = require('./adapter/Active');
const Deal = require('./examples/beforeprinter');
const usb = require("usb");


let info = [];
const buf = [[0x10, 0x04, 0x01],[0x10, 0x04, 0x02],[0x10, 0x04, 0x03],[0x10, 0x04, 0x04]]
/**
 * @type {*[]}
 * style为多列打印的样式,该数组中的每个元素代表一列,每列中的'width'为当前列的在一行中所占的宽度,'align'为当前列中内容的居中样式(left,center,right),最终,所有元素样式中的width相加之和为打印纸的宽度.
 */
const style = [{'width':32,'align':'left'},{'width':7,'align':'right'},{'width':9,'align':'right'}];
const style2 = [{'width':12,'align':'left'},{'width':11,'align':'right'},{'width':13,'align':'left'},{'width':12,'align':'right'}];
const title = [
    {
        'title': '产品',
        'num': '数量',
        'total_price': '金额',
        'description': '------------------------------------------------'
    }
]
/**
 * @param order 需要打印的内容
 * @param printerinfo {type:(usb/net),ip,port:(默认 9100),VID,PID}
 * @param style [{'width':长度,'align':对齐方式}]
 * @constructor
 */
function Printers(order,printerinfo,fun) {
    if(printerinfo.type == 'usb'){
        USB_Printer(order,printerinfo,fun);
    }else if(printerinfo.type == 'net'){
        NET_Printer(order,printerinfo,fun);
    }
}
// 使用USB打印机
function USB_Printer(order,printerinfo,fun) {
    const device  = new escpos.USB(printerinfo.VID,printerinfo.PID);
    const printer = new escpos.Printer(device);

    device.open(function (err) {
        let send_times = 0;
        let recive_times = 1;
        let success = true;
        let fail = false;
        let s_reason = '';
        let f_reason = '';
        // 清空Buffer
        Clear_Buffer(device,function () {
            // 先去检查打印机状态
            let lock = setInterval(function () {
                console.log('send');
                printer.customcheck(buf[send_times]);
                send_times++;
                if(send_times >= 4){
                    send_times = 0;
                    clearInterval(lock);
                    setTimeout(function () {
                        // usb 只能自己主动去查询 ->Active
                        device.read(20,function (err, info) {
                            console.log('this is active',err, info);
                            // 将Buffer分成4份依次处理
                            let arr = eval('('+JSON.stringify(info)+')').data
                            console.log('arr',arr);
                            for(let index in arr){
                                let temp = new Buffer(toHex(arr[index]),'hex');
                                console.log('tamp',temp);
                                console.log('index',parseInt(index)+1);
                                Active(temp,parseInt(index)+1,function (res) {
                                    // 查询过程
                                    if(!res.success){
                                        success = false;
                                        fail = true;
                                    }
                                    s_reason += ','+res.s_reason.toString();
                                    f_reason += ','+res.f_reason.toString();

                                    if( index >= 3){
                                        // 查询打印机状态完成
                                        if(success){
                                            // 打印机没有问题
                                            // to_printer(device,printer,order,style);
                                            printer_TS(device,printer,order,style);
                                            fun({
                                                'success'   :success,
                                                'fail'      :fail,
                                                's_reason'  :s_reason,
                                                'f_reason'  :f_reason
                                            })
                                        }else{
                                            // 打印机有问题
                                            fun({
                                                'success'   :success,
                                                'fail'      :fail,
                                                's_reason'  :s_reason,
                                                'f_reason'  :f_reason
                                            })
                                        }
                                    }
                                });
                            }
                        })
                    },1*1000)
                }
            },0.5*1000)
        })
    })
}
// 使用网络打印机
function NET_Printer(order,printerinfo,fun) {
    // console.log(printerinfo)
    const device  = new escpos.Network(printerinfo.ip,printerinfo.port || 9100);
    const printer = new escpos.Printer(device);
    let active = false;
    let busy = false;
    // 连接打印机
    device.open(function (err) {
        let send_times = 0;
        let recive_times = 1;
        let success = true;
        let fail = false;
        let s_reason = '';
        let f_reason = '';
        // 读取打印机获取打印机状态
        device.read(function (info) {
            // console.log(info);
            // 判断是不是主动查询
            if(active){
                active = false;
                // console.log(info)
                Active(info,recive_times,function (res) {
                    // 查询过程
                    if(!res.success){
                        success = false;
                        fail = true;
                    }
                    s_reason += ','+res.s_reason.toString();
                    f_reason += ','+res.f_reason.toString();

                    if(recive_times >= 4){
                        recive_times = 1;
                        // 查询打印机状态完成
                        if(success){
                            // 打印机没有问题
                            to_printer(device,printer,order,style);
                            fun({
                                'success'   :success,
                                'fail'      :fail,
                                's_reason'  :s_reason,
                                'f_reason'  :f_reason
                            })
                        }else{
                            // 打印机有问题
                            fun({
                                'success'   :success,
                                'fail'      :fail,
                                's_reason'  :s_reason,
                                'f_reason'  :f_reason
                            })
                        }
                    }
                    recive_times++;
                });
            }else{
                if(!busy){
                    busy = true;
                    ASB(info,function (res) {
                        console.log(res);
                        busy = false;
                    });
                }
            }
        })
        // 打印前检查打印机状态
        let lock = setInterval(function () {
            active = true;
            printer.customcheck(buf[send_times]);
            send_times++;
            if(send_times>3){
                send_times = 0;
                clearInterval(lock);
            }
        },0.5*1000)
    })
}
// 打印订单
function to_printer(obj,printer,order,style) {
    // console.log('打印ing')
    let discount_detail = '';
    let description = '';
    Deal().sepa(order.discount_detail,function (text) {
        discount_detail = (text == ''?'暂无优惠':text);
    })
    Deal().sepa(order.description,function (text) {
        description = text;
    })
    printer
        .align('ct')
        .size(1,1)
        .text(order.title + '  外卖订单')
        .size(2,2)
        .text('序号: ' + order.num)
        .size(1,1)
        .text('------------------------------------------------')
        .text(order.storename)
        .text('------------------------------------------------')
        .align('lt')
        .text('订单号: ' + order.orderid)
        .text('第三方订单: ' + order.thr_orderid)
        .text('下单时间: ' + order.ordertime)
        .text('预约时间: ' + order.appointment_time)
        .text('打印时间: ' + order.conf_time)
        .size(2,2)
        .text('顾客姓名: ' + order.username)
        .text('联系电话: ' + order.phone)
        .text('送餐地址: ' + order.address)
        .text('应收现金: '+order.receivable_amount+'元')
        .text('备注: ' + order.remarks)
        .text('发票: ' + order.invoice)
        .size(1,1)
        .text('骑手: ' + order.waiter)
        .text('------------------------------------------------')
        .total_prd(title,style)
        .total_prd(order.productList,style)
        .text('------------------------------------------------')
        .align('rt')
        .text('订单金额: ' + order.order_amout)
        .text('订单优惠: ' + order.order_discount)
        .text('实际支付: ' + order.thr_amout)
        .text('支付状态: ' + order.pay_status)
        .text('------------------------------------------------')
        .align('ct')
        .text(discount_detail)
        .text('------------------------------------------------')
        .text(description)
        .feed(2) // 空2行
        .cut()
        .close()
}
// 打印堂食小票
function printer_TS(obj,printer,order,style) {
console.log('打印堂食小票');

    let discount_detail = '';
    let description = '';
    Deal().sepa(order.discount_detail,function (text) {
        discount_detail = (text == ''?'暂无优惠':text);
    })
    Deal().sepa(order.description,function (text) {
        description = text;
    })
    printer
        .align('ct')
        .size(2,2)
        .text(order.title)
        .feed(1)
        .text(order.istakeout+'   桌号：' + order.table_num)
        .size(1,1)
        .feed(1)
        .text('机号：['+order.pid+']')
        .text('打印时间: ' + order.print_time)
        .text('下单时间: ' + order.print_time)
        .align('lt')
        .text('------------------------------------------------')
        .text('单号: ' + order.transaction_num)
        .text('------------------------------------------------')
        .align('lt')
        .total_prd(title,style)
        .total_prd(order.productList,style)
        .text('------------------------------------------------')
        .total_prd(change_form(order.amount),style2)
        .align('lt')
        .text('支付方式: ' + order.amount.pay_type)
        .text('交易流水号: ' + order.transaction_num)
        .text('------------------------------------------------')
        .align('ct')
        .text(discount_detail)
        .text('------------------------------------------------')
        .text(description)
        .feed(2) // 空2行
        .cut()
        .close();
}
// 将一个数字转化成16进制字符串形式
function toHex(num){
    console.log(num)
    if(typeof num == 'undefined')
        return 'undefined';
    return num<16?num.toString(16).toUpperCase():num.toString(16).toUpperCase();
}
// 清除Buffer
function Clear_Buffer(device,fun) {
    device.read(10,function (err, info) {
        if(info.length != 0){
            console.log('info=',info);
            Clear_Buffer(device,fun);
        }else{
            console.log('清空了')
            fun()
        }
    })
}
// 处理堂食小票计算部分的格式  暂时两个合为一部分
function change_form(obj){
    return [
        {
            title1:'总   价：',
            text1:obj.total_amount,
            title2:'  优   惠：',
            text2:obj.discount_amount
        },
        {
            title1:'应   收：',
            text1:obj.receivable_amount,
            title2:'  实   收：',
            text2:obj.real_amount
        },
        {
            title1:'抹   零：',
            text1:(obj.receivable_amount - obj.real_amount).toFixed(2),
            title2:'  找   零：',
            text2:obj.change_amount
        }
    ]
}

module.exports = Printers