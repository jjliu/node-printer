/**
 * Created by 27260 on 2018/1/4.
 */
'use strict';

let paperwidth = 48;
let CTL_LF = '\x0a';
let CTL_CR = '\x0d';
let CTL_HT = '\x09';
let CTL_VT = '\x0b';
let CTL_2CN = '\x1c\x57\x03';
let CTL_CANCEL_2CN = '\x1c\x57\x00';
let CTL_2EN = '\x1b\x21\x30';
let CTL_CANCEL_2EN = '\x1b\x21\x00';
let CTL_ALIGN_LT = '\x1b\x61\x00';
let CTL_ALIGN_CT = '\x1b\x61\x01';
let CTL_ALIGN_RT = '\x1b\x61\x02';
let CTL_CASH_DRAWER = '\x1b\x70\x00\xee';
let PAPER_PART_CUT = '\x1d\x56\x01';

// 字符串按逗号分隔
function separate(arr,fun) {
    // 处理字符串
    // return str.replace(/,/ig,CTL_LF)
    // 处理数组
    let str = '';
    for(let i in arr){
        if(i == arr.length-1){
            str += arr[i]
        }else{
            str += arr[i] + CTL_LF
        }
    }
    fun(str);
}
// 递归处理
function recursion(products,style) {
    let title = '';
    let more = ['','',''];
    let totalwidth = 0;// 累计长度
    let not_over = false;
    for(let s in style){
        // 所占长度
        let width = style[s].width;
        // 对齐方式
        let align = style[s].align;

        if(products[s] != ''){
            if(strlen(products[s].toString()) > width){
                let temp = {};
                let res = GetStrByWidth(products[s].toString(),0,width)
                title += getStrAlignLeftByWidth(totalwidth, res.str,title,width); // 将计算过的内容放入数组存放
                temp = GetStrByWidth(products[s].toString(),res.index+1,width); // 将超出的部分保存下来
                more[s] = temp.str;
                not_over = true;
            } else {
                if(align == 'left'){
                    // 内部左对齐
                    title += getStrAlignLeftByWidth(totalwidth, products[s].toString(),title,width);
                }else if(align == 'right'){
                    // 内部右对齐
                    title += getStrAlignRightByWidth(totalwidth, products[s].toString(),title,width);
                }else if(align == 'center'){
                    // 内部居中对齐
                    title += getStrAlignCenterByWidth(totalwidth, products[s].toString(),title,width);
                }
            };
        }
        totalwidth += width;
    }
    if(not_over){
        // 存在没有打完的内容
        return title + CTL_LF + recursion(more,style)
    }else{
        return title;
    }
}
/**
 * @param productList 待渲染的数据
 * @param style 渲染数据时候的样式
 * @returns {string}
 */
function total_product (productList,style,fun) {
    let p_text = '';
    for (let p in productList) {
        let product = productList[p];
        let products = []
        for(let tt in product){
            products.push(product[tt]);
        }
        p_text += recursion(products,style) + CTL_LF + productList[p].description ;
    }
    if(typeof fun == "function")
        fun(p_text)
    return p_text;
}
function Deal() {
    let res = {};
    res.total_prd = function (productList,style,fun) {
        total_product(productList,style,fun)
    }
    res.sepa = function (arr,fun) {
        separate(arr,fun)
    }
    return res;
}
// 根据指定位置和长度获取字符串
function GetStrByWidth(str,start,width) {
    let len = 0;
    let res = '';
    for (var i = start; i < str.length; i++) {
        var c = str.charCodeAt(i);
        if ((c >= 0x0001 && c <= 0x007e) || (c >= 0xff60 && c <= 0xff9f)) {
            // 英文字母
            len++;
        }    else {
            // 中文
            len += 2;
        }
        if(len == width){
            res += str[i];
            return {'str':res,'index':i};
        }else if(len > width){
            return {'str':res,'index':i-1};
        }else{
            res += str[i];
        }
    }
    return {'str':res,'index':i-1};
}
// 获取字符串的长度
function strlen (str) {
    var len = 0;
    for (var i = 0; i < str.length; i++) {
        var c = str.charCodeAt(i);
        if ((c >= 0x0001 && c <= 0x007e) || (c >= 0xff60 && c <= 0xff9f)) {
            len++;
        }    else {
            len += 2;
        }
    }
    return len;
}
/**
 * linewidth: 前面的长度 ;  str: 要插入的字符串 ;  other: 之前已经处理过的内容 ;  width:该字符串在该位置的最大空间
 * @param lineWidth
 * @param str
 * @param other
 * @returns {string}
 */
// 靠右
function getStrAlignRightByWidth(lineWidth, str, other,width) {
    // 前面所有内容的长度
    let space = width - strlen(str);
    //计算前面所有的空格
    let spaceStr = '';
    for (var i = 0; i < space; i++) {
        spaceStr += ' ';
    }
    return spaceStr + str;
}
// 靠左
function getStrAlignLeftByWidth(lineWidth, str, other,width) {
    //计算出前面所有的内容长度
    let space = width - strlen(str);
    var spaceStr = '';
    for (var i = 0; i < space; i++) {
        spaceStr += ' ';
    }
    return str + spaceStr;
}
// 居中
function getStrAlignCenterByWidth(lineWidth, str, other,width){
    //计算出前面所有的内容长度
    let space = width - strlen(str);
    var spaceStr = '';
    if(space%2 != 0){
        space--;
        for (var i = 0; i < space/2; i++) {
            spaceStr += ' ';
        }
        return spaceStr + str + spaceStr+' ';
    }else{
        for (var i = 0; i < space/2; i++) {
            spaceStr += ' ';
        }
        return spaceStr + str + spaceStr;
    }

}
module.exports = Deal;