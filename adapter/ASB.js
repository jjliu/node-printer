/**
 * Created by 27260 on 2018/1/6.
 */
// 自动返回
'use strict'
let res = {
    'success'   :true,
    'fail'      :false,
    's_reason'  :[],
    'f_reason'  :[]
}
// 打印机状态集
function toHex(num){//将一个数字转化成16进制字符串形式
    if(num === 'undefined')
        return 'undefined';
    // console.log(num);
    return num<16?"0x0"+num.toString(16).toUpperCase():"0x"+num.toString(16).toUpperCase();
}
let ASB = function (buffer,fun) {
    // console.log(buffer,eval('('+JSON.stringify(buffer)+')').data)
    let [first = 'undefined',second = 'undefined',third = 'undefined',four = 'undefined'] = eval('('+JSON.stringify(buffer)+')').data;
    [first,second,third,four] = [toHex(first),toHex(second),toHex(third),toHex(four)];
    res = {
        'success'   :true,
        'fail'      :false,
        's_reason'  :[],
        'f_reason'  :[]
    };
    First(first,second,third,four,fun);
}
function First(hex,second,third,four,fun) {
    if(hex === 'undefined'){
        fun(res);
        return;
    }
    // console.log(hex);
    // console.log('123123123',hex&&'0x40');
    // console.log(toHex(hex&'0x40'),toHex(hex&'0x20'),toHex(hex&'08'));
    if(toHex(hex&'0x20') === '0x20'){
        res.fail = true;
        res.success = false;
        res.f_reason.push('打印机舱盖被打开')
    }else{
        res.s_reason.push('打印机舱盖关闭')
    }
    if(toHex(hex&'0x08') === '0x08'){
        res.fail = true;
        res.success = false;
        res.f_reason.push('打印机离线')
    }else{
        res.s_reason.push('打印机在线')
    }
    // if(toHex(hex&'0x40') === '0x40'){
    //     res.push('打印纸被送入卷轴')
    // }else{
    //     res.push('打印纸未被送入卷轴')
    // }
    Second(second,third,four,fun);
}
function Second(hex,third,four,fun) {
    if(hex === 'undefined'){
        fun(res);
        return;
    }
    // console.log(hex);
    // console.log('123123123',hex&&'0x40');
    if(toHex(hex&'0x08') === '0x08'){
        res.fail = true;
        res.success = false;
        res.f_reason.push('切纸出现异常')
    }else{
        res.s_reason.push('切纸正常')

        // fun(res);
        // return;
    }
    // if(toHex(hex&'0x40') === '0x40'){
    //     res.push('有自动恢复的错误')
    // }else{
    //     res.push('没有自动恢复的错误')
    // }
    if(toHex(hex&'0x20') === '0x20'){
        res.fail = true;
        res.success = false;
        res.f_reason.push('有不可恢复的错误')
    }else{
        res.s_reason.push('没有不可恢复的错误')
    }
    Third(third,four,fun)
}
function Third(hex,four,fun) {
    if(hex === 'undefined'){
        fun(res);
        return;
    }
    // console.log(hex);
    // console.log('123123123',hex&&'0x40');
    if(toHex(hex&'0x0C') === '0x0C'){
        res.fail = true;
        res.success = false;
        res.f_reason.push('传感器报告纸尽')
    }else{
        res.s_reason.push('传感器报告有纸')
    }
    // if(toHex(hex&'0x03') === '0x03'){
    //     res.push('近端传感器报告纸尽')
    // }else{
    //     res.push('近段传感器报告有纸')
    // }
    Four(four,fun)
}
function Four(hex,fun) {
    if(hex === 'undefined'){
        fun(res);
        return;
    }
    fun(res);
}
module.exports = ASB;