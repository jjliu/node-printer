const Printers = require('node_printer');

// console.log(Printers);
// Printers.Printers(test(),{type:'usb',VID:0x04B8,PID:0x0E0F},function (res) {
Printers.Printers(TS(),{type:'usb',VID:0x04B8,PID:0x0E0F},function (res) {
    if(res.success){
        console.log(res);
        console.log('打印成功!')
    }else{
        console.log(res);
        console.log('打印失败')
    }
})
// Printers.Printers(test(),{type:'net',ip:'172.28.13.111',port:9100},function (res) {
//     if(res.success){
//         console.log(res);
//         console.log('打印成功!')
//     }else{
//         console.log(res);
//         console.log('打印失败')
//     }
// })
function TS() {
    return {
        title: '欢迎光临谷田稻香', // 标题
        istakeout: '外带', // 就餐方式
        table_num: '57', // 桌号
        pid: '1', // 机器号
        print_time: '2017-08-09 12:46:35', // 打印时间
        meal_num: '1002', // 取餐号
        productList: {
            '101':{
                'title': '肉末酸豆角豆浆豆浆豆浆豆浆浆豆浆浆豆浆浆豆浆浆豆浆浆豆浆浆豆浆',
                'num': '2',
                'total_price': '10',
                'description': '（红烧威哥一份）\n（油爆威哥一份）\n（清蒸胖威一份）\n'
            },
            '102':{
                'title': '大红袍(中)浆豆浆浆豆浆v浆豆浆浆豆浆浆豆浆浆豆浆浆豆浆浆豆浆浆豆浆',
                'num': '2',
                'total_price': '10',
                'description': '（红烧威哥一份）\n（油爆威哥一份）\n（清蒸胖威一份）\n'
            },
            '103':{
                'title': '肉末酸豆角',
                'num': 3,
                'total_price': '12',
                'description': ''
            },
            '104':{
                'title': '肉末酸豆角浆+豆浆浆+豆浆浆+豆浆',
                'num': '4',
                'total_price': '202',
                'description': '（红烧威哥一份）\n（油爆威哥一份,清蒸胖威一份）\n'
            },
            '105':{
                'title': '肉末酸豆角',
                'num': '99',
                'total_price': '400',
                'description': '（红烧威哥一份）\n（油爆威哥一份）\n（清蒸胖威一份）\n'
            }
        },
        amount: {
            total_amount: '5560', // 总计 分为单位
            discount_amount: '20000', // 优惠
            receivable_amount: '100', // 应收
            real_amount: '0', // 实付
            change_amount: '0', // 找零
            pay_type: '现金' // 支付方式
        },
        transaction_num: '12312486431843132487', // 交易流水号
        discount_detail: [  // 优惠详情
            // '某某某优惠了什么什么',
            // '某某某优惠了什么什么',
            // '某某某优惠了什么什么',
            // '某某某优惠了什么什么',
            // '某某某优惠了什么什么',
            // '某某某优惠了什么什么',
            // '某某某优惠了什么什么',
            // '某某某优惠了什么什么'
        ],
        description: [  // 票尾描述
            '欢迎到谷田稻香用餐',
            '电话(15710652956)',
            '如您的付款与小票不一致',
            '请拨打财务监督电话(157106529856)'
        ]
    }
}
function test() {
    return {
        title: '上南路测试店',
        num: '饿了吗' + '10',
        storename: '测试店',
        orderid: '1321321354613134646',
        thr_orderid: 'SDFGDFG4545456D4F513135',
        ordertime: '2018-01-04 09:23:12',
        appointment_time: '2018-01-04 09:23:12',
        conf_time: new Date().toLocaleString(), // 打印时间
        username: '测试', // 顾客姓名
        phone: '17612187024', // 顾客电话
        address: '测试测试测试', // 送货地址
        receivable_amount: 0, // 应收现金
        invoice: '不需要发票', // 发票抬头
        remarks: '', // 备注
        waiter: '某某某', // 骑手
        productList: [
            {
                'title': '肉末酸豆角豆浆豆浆豆浆豆浆浆豆浆浆豆浆浆豆浆浆豆浆浆豆浆浆豆浆',
                'num': '2',
                'total_price': '10',
                'description': '（红烧威哥一份）\n（油爆威哥一份）\n（清蒸胖威一份）\n'
            },
            {
                'title': '大红袍(中)浆豆浆浆豆浆v浆豆浆浆豆浆浆豆浆浆豆浆浆豆浆浆豆浆浆豆浆',
                'num': '2',
                'total_price': '10',
                'description': '（红烧威哥一份）\n（油爆威哥一份）\n（清蒸胖威一份）\n'
            },
            {
                'title': '肉末酸豆角',
                'num': 3,
                'total_price': '12',
                'description': ''
            },
            {
                'title': '肉末酸豆角浆+豆浆浆+豆浆浆+豆浆',
                'num': '4',
                'total_price': '202',
                'description': '（红烧威哥一份）\n（油爆威哥一份,清蒸胖威一份）\n'
            },
            {
                'title': '肉末酸豆角',
                'num': '99',
                'total_price': '400',
                'description': '（红烧威哥一份）\n（油爆威哥一份）\n（清蒸胖威一份）\n'
            }
        ],
        order_amout: 100.00,
        order_discount: 2.00,
        discount_detail: [
            '美团优惠5元',
            '红包优惠2元'
        ],
        thr_amout: 70.00,
        pay_status: '未支付',
        description: [
            '欢迎到' + '上南路测试店' + '用餐',
            '电话 (' + '17612187024' + ')',
            '如您的付款与小票不一致',
            '请拨打财务监督电话 (157106529856)'
        ]
    };
}
