/**
 * Created by 27260 on 2018/1/8.
 */
// 主动查询
'use strict'
let res = {
    'success'   :true,
    'fail'      :false,
    's_reason'  :[],
    'f_reason'  :[]
};
function toHex(num){//将一个数字转化成16进制字符串形式
    if(typeof num == 'undefined')
        return 'undefined';
    return num<16?"0x0"+num.toString(16).toUpperCase():"0x"+num.toString(16).toUpperCase();
}
function Active(buf,code,fun) {
    buf = toHex(eval('('+JSON.stringify(buf)+')').data[0]);
    if(buf === 'undefined'){
        res.fail = true;
        res.success = false;
        res.f_reason.push('打印机异常')
        fun(res);
        return;
    }
    res = {
        'success'   :true,
        'fail'      :false,
        's_reason'  :[],
        'f_reason'  :[]
    };
    switch(code){
        case 1:
            if(toHex(buf&'0x08') === '0x08'){
                res.fail = true;
                res.success = false;
                res.f_reason.push('打印机离线')
            }else{
                res.s_reason.push('打印机在线')
            }
            break;
        case 2:
            if(toHex(buf&'0x20') === '0x20'){
                res.fail = true;
                res.success = false;
                res.f_reason.push('没有检测到纸')
            }else{
                res.s_reason.push('打印机有纸')
            }
            if(toHex(buf&'0x04') === '0x04'){
                res.fail = true;
                res.success = false;
                res.f_reason.push('打印机盖被打开')
            }else{
                res.s_reason.push('打印机盖被关闭')
            }
            if(toHex(buf&'0x40') === '0x40'){
                res.fail = true;
                res.success = false;
                res.f_reason.push('打印机出错')
            }
            break;
        case 3:
            if(toHex(buf&'0x08') === '0x08'){
                res.fail = true;
                res.success = false;
                res.f_reason.push('打印机切纸异常')
            }else{
                res.s_reason.push('打印机切纸正常')
            }
            break;
        case 4:
            if(toHex(buf&'0x0C') === '0x0C'){
                res.fail = true;
                res.success = false;
                res.f_reason.push('滚轴近端传感器报告没纸')
            }else{
                res.s_reason.push('滚轴近端传感器报告有纸')
            }
            break;
        default:
            break;
    }
    fun(res);
}
module.exports = Active;